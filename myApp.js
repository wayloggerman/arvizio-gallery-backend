const fs = require("fs");
const path = require("path");
const {
  UPLOADS_FOLDER,
  FILES_PREFIX,
  ASSETS_FOLDER,
  ASSETS_PREFIX,
} = require("./globals/constants");

const cookie = require('@fastify/cookie')
const fastify = require("fastify")({
  logger: true,
  // https: {
  //   key: fs.readFileSync(path.join(__dirname, "key.pem")),
  //   cert: fs.readFileSync(path.join(__dirname, "cert.pem")),
  // },
});
const fastifyStatic = require("fastify-static");

// tools
fastify.register(require("fastify-cors"), { origin: true,  credentials: true }); //manage cors response headers
fastify.register(require("fastify-multipart"), {

  attachFieldsToBody: true,
}); //allows to use multipart post data
fastify.register(fastifyStatic, {
  //allows to download files directly from the specified directory
  root: path.join(__dirname, UPLOADS_FOLDER),
  prefix: FILES_PREFIX,
});
fastify.register(fastifyStatic, {
  root: path.join(__dirname, ASSETS_FOLDER),
  prefix: ASSETS_PREFIX,
  decorateReply: false,
});

// app routes
fastify.register(cookie, {
  secret: "my-secret", // for cookies signature
  parseOptions: {}     // options for parsing cookies
})
fastify.register(require("./routes"), { prefix: "api" });
fastify.register(require('./auth', { prefix: '/' }))



// const fastify = require('fastify')()


// start server listening
const start = async () => {
  try {
    await fastify.listen(8001,'0.0.0.0')
  } catch (err) {
    fastify.log.error(err);
    process.exit(1);
  }
};
start();
