'use strict'

const { UPLOADS_FOLDER } = require("../../globals/constants.js");
const fs = require('fs/promises')
const {
  stringToFile,
  isFileExist,
  readJsonFile,
  objectToFileAsJson
} = require("../../globals/functions");

module.exports = async function (fastify, opts) {
  fastify.post('/:id', async function (request, reply) {
    const data = await request.body
    const id = request.params.id
    const { token } = request.cookies

    const projectPath = `${UPLOADS_FOLDER}/${token}-${id}`
    let settingsPath = `${projectPath}/all-settingss.json`

      if (!(await isFileExist(settingsPath))) {
        await fs.writeFile(settingsPath, JSON.stringify({}))
      }

    const settings = await readJsonFile(settingsPath)
    settings.backLink = data.backLink
    if (settings.backLink)
    await fs.writeFile(settingsPath, JSON.stringify(settings))

    return { status: true, message: 'done' }
  })
}
