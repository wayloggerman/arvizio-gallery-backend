'use strict'

const { UPLOADS_FOLDER } = require('../../globals/constants');
const {createFolders, stringToFile} = require("../../globals/functions");

module.exports = async function (fastify, opts) {
  fastify.post('/', async function (request, reply) {

    let data = await request.body
    const sessionId = data.sessionId.value

    const path = await createFolders(sessionId)

    console.log(path)

    await stringToFile(`${path}/space.json`, await data.file.toBuffer())

    return {status: true, message: 'done'}
  })
}
