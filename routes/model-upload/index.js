"use strict";

const fs = require("fs");
const promfs = require('fs/promises')
const util = require("util");
const { pipeline } = require("stream");
const { UPLOADS_FOLDER } = require('../../globals/constants');
const pump = util.promisify(pipeline);

module.exports = async function (fastify, opts) {
  fastify.post("/:projectName", async function (request, reply) {
    const data = await request.body
    const {token} = request.cookies
    const {projectName} = request.params
    const path = `${UPLOADS_FOLDER}/${token}-${projectName}`

    await promfs.mkdir(path)


    await pump(data.file.toBuffer(), fs.createWriteStream(`${path}/model.fbx`));
    return { status: true, message: "done", path: `files/${path}/model.fbx`};
  });
};
