'use strict'

const fs = require('fs')
const util = require('util')
const {UPLOADS_FOLDER} = require("../../globals/constants.js");
const {
  createFolders,
  extractFileExtension,
  constructThumbFileName,
  takeScreenshotFromVideo,
  createThumbnailForImage,
  stringToFile,
  isFileExist,
  readJsonFile,
  objectToFileAsJson,
  mkDir,
  clearFolder
} = require("../../globals/functions")
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)

module.exports = async function (fastify, opts) {
  fastify.post('/:projectName', async function (request, reply) {

    const data = await request.body
    const {file} = data
    const is360 = data.is360.value === "true" ? true : false

    const {token} = request.cookies
    const {projectName} = request.params


    console.log("data:", file)

    const path = await createFolders(`${token}-${projectName}/`)
    const filePath = `${path}/${file.filename}`

    await pump(file.toBuffer(), fs.createWriteStream(filePath))

    let settingsPath = `${path}/settings.json`
    if (!(await isFileExist(settingsPath))) {
      await stringToFile(settingsPath, "{}")
    }

    const settings = await readJsonFile(settingsPath)
    const c = Object.keys(settings).length
    settings[file.filename] = {
      is360 : is360 === true,
      index: c
    }

    await objectToFileAsJson(settingsPath, settings)

    const ext = extractFileExtension(file.filename)
    switch (ext) {
      case "mp4":
        const thumb = constructThumbFileName(file.filename)
        await takeScreenshotFromVideo(filePath, path, thumb)
        break
      case "jpg":
      case "jpeg":
      case "png":
        await createThumbnailForImage(filePath)
        break
    }

    return {status: true, message: 'done'}
  })

  fastify.post('/photos360/:projectName/:position/:typeOfset', async function (request, reply) {

    const {projectName} = request.params
    const {position} = request.params
    const {typeOfset} = request.params
    const {token} = request.cookies

    const data = await request.body
    const { hasAudio } = data
    const { file } = data
    const { folderName } = data
    const { description } = data

    // console.log({params: request.params, data});
    
    const path = `${UPLOADS_FOLDER}/${token}-${projectName}`
    const filesDirectory = `${path}/${folderName.value}.${typeOfset}`

    // console.log({path, filesDirectory});

    if (position === "start") {
      await createFolders(`${token}-${projectName}`)
      if (!(await isFileExist(filesDirectory))) {
        mkDir(filesDirectory)
      } else {
        // await clearFolder(filesDirectory)
      }

      let settingsPath = `${path}/settings.json`
      if (!(await isFileExist(settingsPath))) {
        await stringToFile(settingsPath, "{}")
      }
      const settings = await readJsonFile(settingsPath)
      const c = Object.keys(settings).length

      const mediaFullName = `${folderName.value}.${typeOfset}`;
      // console.log({mediaFullName});
      const settingObject = settings[mediaFullName]
      // console.log({settingObject});
      if (settingObject) {
        settingObject.hasAudio = hasAudio.value
        settingObject.description = description.value
        settingObject.index = c
      } else {
        settings[mediaFullName] = {hasAudio: hasAudio.value, description:description.value, index: c}
      }
      // console.log({settings});
      await objectToFileAsJson(settingsPath, settings)
    }

    // console.log({file: file.filename});
    const pathToStream = `${filesDirectory}/${file.filename}`
    // console.log({pathToStream});
    await pump(file.toBuffer(), fs.createWriteStream(pathToStream))

    return {status: true, message: 'done'}
  })

  fastify.post('/bg-audio/:projectName/:typeOfset', async function (request, reply) {
    const {token} = request.cookies
    const {projectName} = request.params

    const data = await request.body
    const {file} = data
    const {folderName} = data
    const {typeOfset} = request.params


    const path = `${UPLOADS_FOLDER}/${token}-${projectName}/${folderName.value}.${typeOfset}`

    await pump(file.toBuffer(), fs.createWriteStream(`${path}/${file.filename}-bg-audio.mp3`))

    return {status: true, message: 'done'}
  })
}
