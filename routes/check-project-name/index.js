"use strict";

const { createFolders, deleteFolder } = require("../../globals/functions.js");

module.exports = async function (fastify, opts) {
  fastify.setErrorHandler(function (error, request, reply) {
    reply.send({
      status: false,
      message: error.message,
    });
  });

  fastify.get("/:projectName", async function (request, reply) {
    const projectName = request.params["projectName"];

    const {token} = request.cookies
    try {
    const projectFolder = await createFolders(`${token}-${projectName}`);
    console.log({projectFolder});

    await deleteFolder(projectFolder);
    return { status: true, message: "ok" };
    }
    catch(e){
      return {status: false}
    }
  });
};
