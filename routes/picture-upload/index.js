'use strict'

const fs = require('fs')
const util = require('util')
const {createFolders, fileList, sortPictures, removeLogoFolderFromFileList} = require("../../globals/functions");
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)

module.exports = async function (fastify, opts) {
  fastify.post('/:projectName', async function (request, reply) {

    const data = await request.body
    const sessionId = request.cookies.token
    const projectName = request.params.projectName
    const fields = data


    const path = await createFolders(`${sessionId}-${projectName}`)

    const files = await fileList(path)
    removeLogoFolderFromFileList(files)
    const filtered = files.filter(fn => (fn.indexOf("pic") === 0) )
    sortPictures(filtered)
    const indexes = filtered.map(it => (it.match(/\d+/g)[0] - 0) )
    let firstGap = indexes.length > 0 ? indexes[indexes.length - 1] + 1 : 1
    for (let i = 0; i < indexes.length - 1; i++) {
      if (i === 0 && indexes[0] > 1) {
        firstGap = 1
        break
      }
      if (indexes[i + 1] - indexes[i] > 1) {
        firstGap = indexes[i] + 1
        break
      }
    }
    const filenameSegments = fields.file.filename.split('.')
    const fileExtension = filenameSegments[filenameSegments.length - 1]

    const fname = `pic${firstGap}.${fileExtension}`
    const finishPath = `${path}/${fname}`
     await pump(
       fields.file.toBuffer(),
        fs.createWriteStream(finishPath))

    return {status: true, message: 'done', path: `files/${sessionId}-${projectName}/${fname}`}
  })
}
