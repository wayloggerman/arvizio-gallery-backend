'use strict'

const fs = require('fs')
const util = require('util')
const {UPLOADS_FOLDER} = require("../../globals/constants.js");
const {mkDir} = require("../../globals/functions");
const { pipeline } = require('stream')
const pump = util.promisify(pipeline)

module.exports = async function (fastify, opts) {
  fastify.post('/:projectName', async function (request, reply) {

    const projectName = request.params['projectName']
    const {token} = request.cookies

    const data = await request.body
    const logoFolder = `${UPLOADS_FOLDER}/${token}-${projectName}/project.logo`

    mkDir(logoFolder)
    console.log(data.file);
    await pump(data.file.toBuffer(), fs.createWriteStream(`${logoFolder}/${data.file.fieldname}`))

    return {status: true, message: 'done'}
  })
}
