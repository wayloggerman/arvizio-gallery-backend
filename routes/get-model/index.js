"use strict"

const {UPLOADS_FOLDER, FILES_PREFIX} = require("../../globals/constants");
const {fileList, sortPictures, removeLogoFolderFromFileList} = require("../../globals/functions");

module.exports = async function (fastify) {
  fastify.get("/:id", async function (request) {
    const id = request.params.id
    const token = request.cookies.token
    try {
      const files = await fileList(`${UPLOADS_FOLDER}/${token}-${id}`)
      removeLogoFolderFromFileList(files)

      return {
        status  : true,
        message : "OK",
        model : files.map(filename => (`${FILES_PREFIX}${token}-${id}/${filename}`))
      }
    } catch (e) {
      return {
        status: false,
        message: e.toString()
      }
    }
  })
}
